package DAY1.Assignment;
import java.util.*;
public class PascalsTraingleMemoize {

    public static List<List<Integer>> generate(int numRows) {
        ArrayList<List<Integer>> ans = new  ArrayList<List<Integer>>();
         for(int i=0;i<numRows;i++){
             ArrayList<Integer> tem = new ArrayList<>();
             for(int j=0;j<=i;j++){
                 if(j==0 || j==i) tem.add(1);
                 else{
                     int a = ans.get(i-1).get(j-1);
                     int b = ans.get(i-1).get(j);
                     tem.add(a+b);
                 }
             }
             ans.add(tem);
         }
         return ans;
     }

    public static void main(String[] args) {
        
       
        int numRows = 5;
        List<List<Integer>> result = generate(numRows);
        
        // Print the generated triangle
        for (List<Integer> row : result) {
            for (int num : row) {
                System.out.print(num + " ");
            }
            System.out.println();
        }
    }
}
