package DAY1.Assignment;
import java.util.*;
public class PascalTriangleIter {

    public static void main(String[] args) {
		
		int row = 5;
		
		pascalTriangle(row);
		
	}
	public static void pascalTriangle(int num){
		
		for(int i=0;i<num;i++){
			
			for(int j=num;j>i;j--){
				System.out.print(" ");
			}
			int value=1;
	
			for(int j=0;j<=i;j++){
				System.out.print(value+" ");
				value = value*(i-j);
                value = value /(j+1);
			}
			System.out.print("\n");
		}	
	}
}
