package DAY1.Assignment;
import java.util.*;

public class PascalsTriangleRecursion {
    public static void main(String[] args) {

	    int row = 5;
	    printPattern(row);

	}

	public static void printPattern(int num) {
		for(int i = 0; i < num; i++) {

			for(int j=num ; j>i; j--){
				System.out.print(" ");
			}

			
	        for(int j = 0; j <= i; j++) {
	            System.out.print(pascalTriangle(i, j) + " ");
	        }
	        System.out.println();
		}
	}
	public static int pascalTriangle(int i, int j) {
		
		if(j == 0 || j == i){
			return 1;
		}
	    else{ 
	    	return pascalTriangle(i - 1, j - 1) + pascalTriangle(i - 1, j);
	    }
	     
	   }
}
