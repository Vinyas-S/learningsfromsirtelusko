## What is recursion?

-Recursion is a programming concept that refers to the ability of a function or method to call itself during its execution.

-When a recursive function is called, it performs some computations and then calls itself with a smaller input or a modified version of the original input. This process continues until a base case is reached, which is a condition that stops the recursion and allows the function to start returning values.

## Where can recursion help us?
-If you have a very complex problem in hand, recursion allows you to break down complex problems into smaller, more manageable subproblems. By solving these subproblems recursively, you can gradually build up the solution to the original problem.

-For solving recursive structures like Trees, Graphs or nested data structures.

-Recursive solutions often result in shorter and more elegant code compared to their iterative solutions

## Don't think recursion is God, it also has disadvantages. Know here.

-Recursive functions can consume more memory due to the recursion stack, and certain problems may have more efficient iterative solutions

- For example: Take a problem of factorial of a number to understand this better.

Factorial is a mathematical procedure that computes the sum of all positive integers ranging from 1 to a given number.

-The recursion which calls are made to calculate the factorial are stored in the stack memory.

Here is the recursion calls: - 

![Recursion call](recursioncalls.png)

-What if the stack overflow happens; ie. if the calls made exceeds the size of the stack memory? Just like this:-

![Stack Overflow](StackOverFlowError.jpg)

We will have to employ a new method of solving problem.

That is where Memoization comes.

# What is Memoization 

-Known as the “top-down” dynamic programming, usually the problem is solved in the direction of the main problem to the base cases.
-We tend to store the value of sub problems which we have solved in one recursion call in some Map/Array
-So when we again encounter the same sub problem,  we can use the already stored result of the sub problem.


## Steps to memoize a recursive solution:

Any recursive solution to a problem can be memoized using these three steps:

1. Create a dp[n+1] array initialized to -1.
2. Whenever we want to find the answer of a particular value (say n), we first check whether the answer is already calculated using the dp array(i.e dp[n]!= -1). If yes, simply return the value from the dp array.
3. If not, then we are finding the answer for the given value for the first time, we will use the recursive relation as usual but before returning from the function, we will set dp[n] to the solution we get.

