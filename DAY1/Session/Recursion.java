package DAY1.Session;

import java.util.*;
import java.util.HashMap;

/*
 * Factorial of a number
 * 5! = 5 * 4 * 3 * 2 * 1
 *    = 120
 * 
 * 
 */
public class Recursion {

    public static int findFactorial(int num){
        /* 
        Iterative approach
        int result = 1;
        for(int i=1 ; i<=num ; i++){
            result*=i;
        }
        */


        //Recursive approach
        if(num==1) return 1;

        return num * findFactorial(num-1);
        
    }


    public static int findFibonacci(int num){
        
        if(num <= 1) return num;

        /* Iterative approach
        int num1 = 0;
        int num2 = 1;
        int num3 = 0;
        for(int i=2 ; i <= num; i++){
            num3 = num2 + num1;
            num1 = num2;
            num2 = num3;
        }
        return num3;
        */


        /* Recursive approach 
        int last = findFibonacci(num-1);
        int secondLast = findFibonacci(num-2);
        return last +secondLast;
        */

        // Memoization
        int dp[]=new int[num+1];
        Arrays.fill(dp,-1);

        if(dp[num]!= -1) return dp[num];
        return dp[num]= findFibonacci(num-1) + findFibonacci(num-2);
    }

    public static void main(String[] args){
        int num = 9;
        // System.out.println(findFactorial(num));
        System.out.println(findFibonacci(num));
    }

}
